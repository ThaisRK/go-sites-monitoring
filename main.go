package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

const monitoringTimes = 5
const delay = 5

// funcoes de pacote externo comecam com letra maiuscula
// deve ter uma funcao com o mesmo nome do pacote
func main() {
	start()
	// readSiteFiles()
	for {
		showMenu()

		// name, age := returnNameAndAge()
		// -----> Quando não quero utilizar um dos retornos da funçao,
		// usar o identificador em branco (underline)
		// _, age := returnNameAndAge()
		// fmt.Print("Tenho ", age, " anos")

		cmd := readCmd()

		switch cmd {
		case 1:
			startMonitoring()
		case 2:
			fmt.Println("Exibindo logs...")
			printLogs()
		case 0:
			fmt.Println("Saindo...")
			os.Exit(0)
		default:
			fmt.Println("Não conheço esse comando...")
			os.Exit(-1)
		}

	}
}
func start() {
	// var name string = "Thais"
	// var version float32 = 1.1
	// var age int

	// ---> inferencia de tipo:
	// var name = "Thais"
	// var version = 1.1
	// var age = 29
	// fmt.Println("Tipo var version:", reflect.TypeOf(version))
	// fmt.Println("Tipo var name:", reflect.TypeOf(name))

	// ----> operador de inferencia de variaveis curto
	name := "Thais"
	version := 1.2

	fmt.Println("Hey, sra.", name)
	fmt.Println("Version:", version)
}
func readCmd() int {
	// Scanf(formatador, ponteiroDaVariavel)
	// o que entrar no %d será alocado na var cujo pondeiro foi passadao
	// fmt.Scanf("%d", &cmd)
	var cmdInput int

	fmt.Scan(&cmdInput)
	fmt.Println("Comando escolhido: ", cmdInput)

	return cmdInput
}
func showMenu() {
	fmt.Println("1 - Iniciar monitoramento")
	fmt.Println("2 - Exibir logs")
	fmt.Println("0 - Sair")
}
func startMonitoring() {
	fmt.Println("Monitorando...")

	sites := readSiteFiles()
	// for i := 0; i < len(sites); i++ {
	// 	fmt.Println(sites[i])
	// }

	for i := 0; i < monitoringTimes; i++ {
		for i, site := range sites {
			fmt.Println("Posição", i, "Site ", site)
			testSite(site)
		}
		time.Sleep(delay * time.Second)
	}
}
func testSite(site string) {
	resp, err := http.Get(site)

	if err != nil {
		fmt.Println("Erro: ", err)
	}

	if resp.StatusCode == 200 {
		fmt.Println("O site", site, " foi carregado com sucesso")
		registerLog(site, true)

	} else {
		fmt.Println("O site", site, " está com problemas. STATUS CODE:", resp.StatusCode)
		registerLog(site, true)
	}
}
func readSiteFiles() []string {
	var sites []string
	file, err := os.Open("sites.txt")
	// file, err := ioutil.ReadFile("sites.txt")

	if err != nil {
		fmt.Println("Erro:", err)
	}
	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')
		line = strings.TrimSpace(line)
		fmt.Println("linha: ", line)

		sites = append(sites, line)
		if err == io.EOF {
			break
		}
	}

	fmt.Println("slice de sites", sites)
	file.Close()
	return sites
}

func registerLog(site string, status bool) {
	file, err := os.OpenFile("log.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		fmt.Println(err)
	}

	file.WriteString(time.Now().Format("02/01/2006 15:04:05") + " - " + site + " - online: " + strconv.FormatBool(status) + "\n")

	file.Close()
}

func printLogs() {
	file, err := ioutil.ReadFile("log.txt")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(file))
}

// func returnNameAndAge() (string, int) {
// 	name := "Thais :)"
// 	age := 29
// 	return name, age
// }

// para compilar e gerar o executável:
// go build hello.go
// para executar:
// ./hello

// OU

// compilar e executar na mesma linha
// go run hello.go
